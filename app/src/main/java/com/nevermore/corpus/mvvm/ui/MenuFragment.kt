package com.nevermore.corpus.mvvm.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.nevermore.corpus.R
import com.nevermore.corpus.data.Service
import com.nevermore.corpus.recycler.PostsAdapter
import kotlinx.android.synthetic.main.fragment_menu.*

class MenuFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupNavigationMenu()
        setupToolbar()
        setupRecycler()
    }

    private fun setupToolbar() {
        toolbar.apply {
            (activity!! as AppCompatActivity).setSupportActionBar(this)
            setNavigationOnClickListener {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun setupNavigationMenu() {
        navigation.apply {
            setNavigationItemSelectedListener { menuItem ->
                menuItem.isChecked = true
                drawerLayout.closeDrawers()
                when (menuItem.itemId) {
                    //R.id.nav_lang -> LanguageDialog().show(supportFragmentManager, "lang")
                }
                false
            }
        }
    }

    private fun setupRecycler(){
        recycler.apply {
            adapter = PostsAdapter().apply { items = MutableList(10){ Service() } }
            layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        }
    }

    fun isMenuDrawerOpened(): Boolean {
        return drawerLayout?.isDrawerOpen(GravityCompat.START) ?: false
    }

    fun closeMenuDrawer() {
        drawerLayout?.closeDrawers()
    }
}