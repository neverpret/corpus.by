package com.nevermore.corpus.mvvm.repositories

import io.reactivex.disposables.CompositeDisposable

abstract class BaseRepository {
    protected val subscriptions = CompositeDisposable()

    open fun clear() {
        subscriptions.clear()
    }
}