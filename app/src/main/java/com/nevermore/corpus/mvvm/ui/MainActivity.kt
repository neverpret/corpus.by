package com.nevermore.corpus.mvvm.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.nevermore.corpus.R
import com.nevermore.corpus.mvvm.repositories.MainRepository
import com.nevermore.corpus.mvvm.viewmodels.MainViewModel
import com.nevermore.corpus.navigation.MainNavigator
import com.nevermore.corpus.navigation.MainScreens
import ru.terrakok.cicerone.Cicerone


class MainActivity : AppCompatActivity() {
    private val navigator = MainNavigator(this, R.id.container)
    private lateinit var vm: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vm = ViewModelProviders.of(this).get(MainViewModel::class.java)
        if (savedInstanceState == null) {
            vm.initRepository(
                MainRepository(Cicerone.create())
            )
        }
        vm.router.newRootScreen(MainScreens.MENU_SCREEN)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        vm.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        vm.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        supportFragmentManager.findFragmentById(R.id.container).apply{
            if(this is MenuFragment){
                if(isMenuDrawerOpened()) closeMenuDrawer()
            } else vm.router.exit()
        }
    }
}
