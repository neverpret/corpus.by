package com.nevermore.corpus.mvvm.viewmodels

import com.nevermore.corpus.mvvm.repositories.MainRepository
import com.nevermore.corpus.mvvm.viewmodels.BaseViewModel
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

class MainViewModel : BaseViewModel<MainRepository>() {
    val router: Router
        get() = repository!!.cicerone.router
    val navigatorHolder: NavigatorHolder
        get() = repository!!.cicerone.navigatorHolder
}