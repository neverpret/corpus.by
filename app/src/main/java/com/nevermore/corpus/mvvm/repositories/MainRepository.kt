package com.nevermore.corpus.mvvm.repositories

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class MainRepository(
    val cicerone: Cicerone<Router>
) : BaseRepository() {

}