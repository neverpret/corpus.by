package com.nevermore.corpus.mvvm.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import com.nevermore.corpus.R
import com.nevermore.corpus.mvvm.viewmodels.MainViewModel
import com.nevermore.corpus.tools.hideKeyboard
import com.nevermore.corpus.tools.isGone
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_base.*

abstract class BaseFragment : Fragment() {
    protected abstract val contentLayoutID: Int
    protected open val toolbarTitleStrID : Int? = null
    protected open val isBackable = true
    protected open val isToolbarVisible = true
    protected open val inputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    protected lateinit var mainVM: MainViewModel
    protected val subscriptions = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainVM = ViewModelProviders.of(activity!!).get(MainViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_base, container, false).apply {
            toolbar.apply {
                title = ""
                toolbarTitleStrID?.let{
                    tvTitle.text = resources.getString(it)
                }

                (activity!! as AppCompatActivity).setSupportActionBar(this)
                isGone(!isToolbarVisible)

                if (!isBackable) {
                    navigationIcon = null
                }
                setNavigationOnClickListener { activity!!.onBackPressed() }
            }
            activity!!.window.setSoftInputMode(inputMode)
            inflater.inflate(contentLayoutID, contentHolder, true)
        }
    }

    protected fun showMessage(message : String){
        Toast.makeText(activity!!, message, Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        activity!!.hideKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.clear()
    }
}