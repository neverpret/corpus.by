package com.nevermore.corpus.retrofit

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface PusherCommentsService {

    @GET("comment/")
    fun getComments(
        @Query("api_token") apiToken: String,
        @Query("page") page: Int
    ): Observable<Response<ResponseBody>>

    @GET("comment/{commentID}/answer/")
    fun getAnswersFor(
        @Path("commentID") commentID: Int,
        @Query("api_token") apiToken: String
    ): Observable<Response<ResponseBody>>


    @POST("comment/{commentID}/answer/")
    fun storeAnswer(
        @Path("commentID") commentID: Int
    ): Observable<Response<ResponseBody>>

    @POST("comment/")
    fun storeCommnet(
    ): Observable<Response<ResponseBody>>

}