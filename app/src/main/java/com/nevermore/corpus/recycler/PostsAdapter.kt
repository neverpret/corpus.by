package com.nevermore.corpus.recycler

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.nevermore.corpus.data.Service

class PostsAdapter() : RecyclerView.Adapter<PostViewHolder>() {
    var items = mutableListOf<Service>()
    var onClick: ((service: Service?) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder(parent).apply {
            onClick = this@PostsAdapter.onClick
        }
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}