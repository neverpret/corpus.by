package com.nevermore.corpus.recycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.nevermore.corpus.R
import com.nevermore.corpus.data.Service


class PostViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_service, parent, false)
    ) {
    var service : Service? = null
    var onClick: ((Service?) -> Unit)? = null

    init {
        itemView.setOnClickListener { onClick?.invoke(service) }
    }
    fun bind(item : Service){
        service = item
    }
}